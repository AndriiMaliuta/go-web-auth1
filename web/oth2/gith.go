package oth2

import (
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/github"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

var gitOathConfig = &oauth2.Config{
	ClientID:     os.Getenv("GIT_CL_ID"),
	ClientSecret: os.Getenv("GIT_OAUTH_SEC"),
	Endpoint:     github.Endpoint,
}

func StartGitOauthPage(wr http.ResponseWriter, r *http.Request) {
	htmlTmp := `<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Home</title>
	</head>
	<body>
		<form action="/oauth/github" method="post">
        	<input type="submit" name="login" id="login" value="Login with GitHub">
    	</form>
	</body>
	</html>`
	fmt.Fprint(wr, htmlTmp)
}

//var gitLoginAttempts = map[string]string

func StartGitOauth(wr http.ResponseWriter, r *http.Request) {
	redURL := gitOathConfig.AuthCodeURL("0000")
	http.Redirect(wr, r, redURL, http.StatusSeeOther)
}

func CompleteGitOauth(wr http.ResponseWriter, req *http.Request) {
	code := req.FormValue("code")
	state := req.FormValue("state")
	if state != "0000" {
		http.Error(wr, "State is incorrect", http.StatusBadRequest)
		return
	}
	token, err := gitOathConfig.Exchange(req.Context(), code)
	if err != nil {
		http.Error(wr, "Couldn't login", http.StatusInternalServerError)
		return
	}

	ts := gitOathConfig.TokenSource(req.Context(), token)
	client := oauth2.NewClient(req.Context(), ts)

	// use CLIENT
	reqBody := strings.NewReader(`{ "query": "{query {viewer {id}}}" }`)
	resp, err :=
		client.Post("https://api.github.com/graphql", "application/json", reqBody)
	if err != nil {
		http.Error(wr, "Could not get user", http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	log.Println(string(bs))

}

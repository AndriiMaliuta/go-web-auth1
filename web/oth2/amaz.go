package oth2

import (
	"github.com/google/uuid"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/amazon"
	"net/http"
	"time"
)

//
type user struct {
	password []byte
	First    string
}

var AmazOauth = &oauth2.Config{
	ClientID:     "",
	ClientSecret: "",
	Endpoint:     amazon.Endpoint,
	RedirectURL:  "http://localhost;8080/oauth/receive",
}

var AmazDb = map[string]user{}

var AmazSession = map[string]time.Time{}

var Endpoint = oauth2.Endpoint{
	AuthURL:  "",
	TokenURL: "",
}

func AmazonLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	id := uuid.New().String()
	AmazSession[id] = time.Now().Add(time.Hour)
	http.Redirect(w, r, AmazOauth.RedirectURL, http.StatusSeeOther)

}

package web

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha512"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"io"
	"time"
)

//var key = []byte{1, 2, 3}

type key struct {
	key     []byte
	created time.Time
}

var currentKid = "some KID"
var keys = map[string]key{}

func SignMessage(msg []byte) ([]byte, error) {
	h := hmac.New(sha512.New, keys[currentKid].key)
	_, err := h.Write(msg)
	if err != nil {
		return nil, fmt.Errorf("error")
	}
	signature := h.Sum(nil)
	return signature, nil
}

func CreateToken(claims UserClaims) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	signToken, err := token.SignedString(keys[currentKid].key)
	if err != nil {
		return ""
	}
	return signToken
}

func ParseToken(sToken string) *UserClaims {
	claims := &UserClaims{}
	t, err := jwt.ParseWithClaims(sToken, claims, func(t *jwt.Token) (interface{}, error) {
		if t.Method.Alg() != jwt.SigningMethodHS512.Alg() {
			return nil, fmt.Errorf("invalid signing algorythm")
		}
		kid, ok := t.Header["kid"].(string)
		if !ok {
			return nil, fmt.Errorf("Invalid key ID")
		}
		k, ok := keys[kid]

		return k.key, nil
	})
	if err != nil {
		return nil
	}
	if !t.Valid {
		return nil
	}
	return t.Claims.(*UserClaims)
}

func GenerateNewKey() error {
	newKey := make([]byte, 64)
	_, err := io.ReadFull(rand.Reader, newKey)
	if err != nil {
		return fmt.Errorf("Error in generating new JWT key")
	}
	uid, err := uuid.NewUUID()
	if err != nil {
		return err
	}
	keys[uid.String()] = key{
		key:     newKey,
		created: time.Now(),
	}
	return nil
}

func JwtGen() {
	myKey := []byte("MyKey")
	claims := UserClaims{
		"",
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(5 * time.Minute).Unix(),
			Issuer:    "Test",
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	signedStr, err := token.SignedString(myKey)
	fmt.Printf("%v %v", signedStr, err)
}

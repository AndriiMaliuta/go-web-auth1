package web

import (
	"fmt"
	"net/http"
)

func HtmlCats(wr http.ResponseWriter, r *http.Request) {
	htmlTmp := `<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Home</title>
	</head>
	<body>
		<h2>Cats HTML</h2>
	</body>
	</html>`
	fmt.Fprint(wr, htmlTmp)
}

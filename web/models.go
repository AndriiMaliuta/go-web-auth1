package web

import "github.com/golang-jwt/jwt"

type UserClaims struct {
	Foo string `json:"foo"`
	jwt.StandardClaims
}

package main

import (
	"fmt"
	"go-web-auth1/web"
	"go-web-auth1/web/oth2"
	_ "golang.org/x/crypto/bcrypt"
	"net/http"
)

func cats(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "Cats page")
}

func main() {

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, "Hello!")
	})

	http.HandleFunc("/cats", cats)
	http.HandleFunc("/cats-html", web.HtmlCats)
	http.HandleFunc("/oauth/github", oth2.StartGitOauth)
	http.HandleFunc("/oauth2/receive", oth2.CompleteGitOauth)

	err := http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		return
	}
}
